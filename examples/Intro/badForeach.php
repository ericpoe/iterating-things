<?php

$str = "Hello World";

echo "Expect this to fail!" . PHP_EOL;

foreach ($str as $char) {
    echo $char . " ";
}
