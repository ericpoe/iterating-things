<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\Iterator\ForeachString;

$text = isset($argv[1])
    ? new ForeachString($argv[1])
    : new ForeachString("Hello, world!");

foreach ($text as $char) {
    echo $char . " ";
}

echo PHP_EOL;
