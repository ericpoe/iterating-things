<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\Iterator\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("This is fine. ☕");

foreach ($text as $char) {
    echo $char . " ";
}

echo PHP_EOL;
