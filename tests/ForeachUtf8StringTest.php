<?php
namespace IteratingThings\Tests;

use IteratingThings\Iterator\ForeachUtf8String;
use IteratingThings\IteratorAggregate\ForeachUtf8String as IAForeachUtf8String;
use IteratingThings\IteratorAggregateIterator\ForeachUtf8String as ComboForeachUtf8String;

class ForeachUtf8StringTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider textProvider
     */
    public function testForeach(string $text, array $expected)
    {
        $str = new ForeachUtf8String($text);

        $strArr = [];
        foreach ($str as $char) {
            $strArr[] = $char;
        }

        $this->assertSame($expected, $strArr);
    }

    /**
     * @dataProvider textProvider
     */
    public function testIteratorAggregatorForeach(string $text, array $expected)
    {
        $str = new IAForeachUtf8String($text);

        $strArr = [];
        foreach ($str as $char) {
            $strArr[] = $char;
        }

        $this->assertSame($expected, $strArr);
    }

    /**
     * @dataProvider textProvider
     */
    public function testIteratorAggregatorIteratorForeach(string $text, array $expected)
    {
        $str = new ComboForeachUtf8String($text);
        $it = $str->getIterator();

        $strArr = [];
        foreach ($it as $char) {
            $strArr[] = $char;
        }

        $this->assertSame($expected, $strArr);
    }

    public function textProvider() : array
    {
        return [
            "ascii" => ["Sunshine", ["S", "u", "n", "s", "h", "i", "n", "e"]],
            "utf-8" => ["本情次見闘", ["本", "情", "次", "見", "闘"]],
            "utf-8Emoji" => ["😜😘😆", ["😜","😘","😆"]],
        ];
    }
}
