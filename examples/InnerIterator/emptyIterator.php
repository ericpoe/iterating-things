<?php
declare(strict_types=1);

include(__DIR__ . "/../../vendor/autoload.php");

use \IteratingThings\Iterator\ForeachUtf8String;

$obj = new \ArrayObject(range(1, 5));
iterate($obj);

$obj = new ForeachUtf8String("Howdy!");
iterate($obj);

function iterate(iterable $thing) : void
{
    printf("Attempting to iterate over an iterable: %s\n", get_class($thing));

    foreach (getIterator($thing) as $item) {
        echo $item . " ";
    }

    echo PHP_EOL;
}

function getIterator(iterable $iterable) : \Traversable
{
    $iterator = null;

    switch ($iterable) {
        case is_array($iterable):
            $iterator = new \ArrayIterator($iterable);
            break;
        case $iterable instanceof ForeachUtf8String:
            $iterator = $iterable;
            break;
        default:
            echo "I hadn't planned on how to utilize this kind of iterable thing!" . PHP_EOL;
            $iterator = new \EmptyIterator();
    }

    return $iterator;
}
