<?php
declare(strict_types=1);

namespace IteratingThings\OuterIterator;

class ConsonantFilterIterator extends \FilterIterator
{
    /**
     * Check whether the current element of the iterator is acceptable
     *
     * @link  http://php.net/manual/en/filteriterator.accept.php
     * @return bool true if the current element is acceptable, otherwise false.
     * @since 5.1.0
     */
    public function accept() : bool
    {
        $encoding = "UTF-8";
        $vowels = "aeiou";
        $current = mb_strtolower(parent::current(), $encoding);

        return false === mb_strpos($vowels, $current, 0, $encoding);
    }
}
