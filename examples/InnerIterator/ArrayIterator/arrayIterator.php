<?php
$SplIterators = [
    "AppendIterator",
    "ArrayIterator",
    "CachingIterator",
    "CallbackFilterIterator",
    'DirectoryIterator (extends "SplFileInfo")',
    "EmptyIterator",
    "FileSystemIterator",
    "FilterIterator",
    "GlobIterator",
    "InfiniteIterator",
    "IteratorIterator",
    "LimitIterator",
    "MultipleIterator",
    "NoRewindIterator",
    "ParentIterator",
    "RecursiveArrayIterator",
    "RecursiveCachingIterator",
    "RecursiveCallbackFilterIterator",
    "RecursiveDirectoryIterator",
    "RecursiveFilterIterator",
    "RecursiveIterator",
    "RecursiveIteratorIterator",
    "RecursiveRegexIterator",
    "RecursiveTreeIterator",
    "RegexIterator",
    "SeekableIterator",
];

echo "The SPL Iterators are: " . PHP_EOL;

$it = new \ArrayIterator($SplIterators);

foreach ($it as $key => $line) {
    echo $key +1 . ": " . $line . PHP_EOL;
}
