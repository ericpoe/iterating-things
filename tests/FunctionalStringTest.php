<?php
namespace IteratingThings\Tests;

use IteratingThings\FunctionalMethods;
use IteratingThings\Iterator\FunctionalString;
use IteratingThings\IteratorAggregate\FunctionalString as IAFunctionalString;
use IteratingThings\IteratorAggregateIterator\FunctionalString as ComboFunctionalString;

class FunctionalStringTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider mapProvider
     */
    public function testStringMap(FunctionalMethods $input, $expected)
    {
        $capitalized = $input->map(function ($char) {
            return mb_convert_case($char, MB_CASE_UPPER, mb_detect_encoding($char));
        });

        $this->assertEquals($expected, $capitalized);
    }

    public function mapProvider()
    {
        return [
            "Iterator" => [new FunctionalString("stuff"), new FunctionalString("STUFF")],
            "IteratorAggregate" => [new IAFunctionalString("stuff"), new IAFunctionalString("STUFF")],
            "IteratorAggregateIterator" => [(new ComboFunctionalString("stuff"))->getIterator(), new ComboFunctionalString("STUFF")],
        ];
    }

    /**
     * @dataProvider reduceProvider
     */
    public function testStringReduce(FunctionalMethods $input, $expected)
    {
        $convmap = [0x0, 0xffff, 0, 0xffff]; // gamut of UTF-8 space

        $bigNum = $input->reduce(function ($carry, $char) use ($convmap) {
            $temp = mb_encode_numericentity($char, $convmap, 'UTF-8');
            $num = empty($temp) ? 0 : intval(substr($temp, 2, -1));

            return $carry + $num;
        });

        $avg = intval(floor($bigNum / mb_strlen($input, 'UTF-8')));

        $avgChar = mb_decode_numericentity(sprintf("&#%s;", $avg), $convmap, 'UTF-8');

        $this->assertEquals($avgChar, $expected);
    }

    public function reduceProvider()
    {
        return [
            "Iterator" => [new FunctionalString("stuff"), new FunctionalString("n")],
            "IteratorAggregate" => [new IAFunctionalString("stuff"), new IAFunctionalString("n")],
            "IteratorAggregateIterator" => [(new ComboFunctionalString("stuff"))->getIterator(), new ComboFunctionalString("n")],
        ];
    }

    /**
     * @dataProvider filterProvider
     */
    public function testStringFilter(FunctionalMethods $input, $expected)
    {
        $vowels = new FunctionalString("aeiou");

        $vowelsInWord = $input->filter(function ($char) use ($vowels) {
            foreach ($vowels as $vowel) {
                if ($char === $vowel) {
                    return true;
                }
            }
            return false;
        });

        $this->assertEquals($expected, $vowelsInWord);
    }

    public function filterProvider()
    {
        $str = "stuffiness";
        $vowels = "uie";

        return [
            "Iterator" => [new FunctionalString($str), new FunctionalString($vowels)],
            "IteratorAggregate" => [new IAFunctionalString($str),new IAFunctionalString($vowels)],
            "IteratorAggregateIterator" => [(new ComboFunctionalString($str))->getIterator(), new ComboFunctionalString($vowels)],
        ];
    }
}
