<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregateIterator;

use IteratingThings\FunctionalMethods;

class FunctionalString implements \IteratorAggregate
{
    /** @var string */
    protected $encoding;

    /** @var string */
    protected $str;

    public function __construct(string $str)
    {
        $this->encoding = 'UTF-8';
        $this->str = mb_convert_encoding($str, $this->encoding);
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : FunctionalStringIterator
    {
        return new FunctionalStringIterator($this->str);
    }

    public function __toString() : string
    {
        return ($this->str);
    }
}
