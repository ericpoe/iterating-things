<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregate;

use IteratingThings\FunctionalMethods;

class FunctionalString implements \IteratorAggregate, FunctionalMethods
{
    /** @var string */
    protected $encoding;

    /** @var array */
    protected $str;

    public function __construct(string $str)
    {
        $this->encoding = 'UTF-8';
        $this->str = preg_split('//u', mb_convert_encoding($str, $this->encoding), 0, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->str);
    }

    public function map(\Closure $closure) : self
    {
        $str = '';
        foreach ($this as $char) {
            $str .= $closure($char);
        }

        return new self($str);
    }

    /**
     * @param \Closure    $closure
     * @param mixed|null  $carry
     * @return FunctionalString|mixed|null
     */
    public function reduce(\Closure $closure, $carry = null)
    {
        foreach ($this as $char) {
            $carry = $closure($carry, $char);
        }

        return is_string($carry) ? new self($carry) : $carry;
    }

    public function filter(\Closure $closure) : self
    {
        $filtered = new self('');
        foreach ($this as $letter) {
            if ($closure($letter)) {
                $filtered = new self($filtered . $letter);
            }
        }
        return $filtered;
    }

    public function __toString() : string
    {
        return implode('', $this->str);
    }
}
