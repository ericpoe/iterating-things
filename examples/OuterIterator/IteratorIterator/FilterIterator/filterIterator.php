<?php
include(__DIR__ . "/../../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;
use \IteratingThings\OuterIterator\ConsonantFilterIterator;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The quick brown fox jumps over the lazy dogs");

$it = $text->getIterator();
$consonantIterator = new ConsonantFilterIterator($it);

foreach ($consonantIterator as $char) {
    echo $char;
}

echo PHP_EOL;
