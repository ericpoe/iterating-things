<?php
declare(strict_types=1);

namespace IteratingThings\Iterator;

class ForeachUtf8String implements \Iterator
{
    /** @var string */
    protected $encoding;

    /** @var string */
    protected $str;

    /** @var int */
    protected $ptr;

    public function __construct(string $str)
    {
        $this->encoding = 'UTF-8';
        $this->str = mb_convert_encoding($str, $this->encoding);
        $this->ptr = 0;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function current() : string
    {
        return mb_substr($this->str, $this->ptr, 1, $this->encoding);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     */
    public function next() : void
    {
        $this->ptr++;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     */
    public function key() : int
    {
        return $this->ptr;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     */
    public function valid() : bool
    {
        return $this->ptr < mb_strlen($this->str);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     */
    public function rewind() : void
    {
        $this->ptr = 0;
    }

    public function __toString() : string
    {
        return $this->str;
    }
}
