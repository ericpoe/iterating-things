<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The quick brown fox jumps over the lazy dogs ");

$it = $text->getIterator();
$infiniteIt = new InfiniteIterator($it);

// Print over and over again, but stop after 95 chars
foreach (new LimitIterator($infiniteIt, 0, 95) as $char) {
    echo $char;
}

echo PHP_EOL;
