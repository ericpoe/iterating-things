<?php
include(__DIR__ . "/../../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The quick brown fox jumps over the lazy dogs");

$encoding = 'UTF-8';
$vowels = mb_convert_encoding("aeiou", $encoding);
$it = $text->getIterator();

$consonants = new \CallbackFilterIterator($it, function ($char) use ($vowels, $encoding) {
    return false === mb_strpos($vowels, mb_strtolower($char, $encoding), 0, $encoding);
});

foreach ($consonants as $char) {
    echo $char;
}

echo PHP_EOL;
