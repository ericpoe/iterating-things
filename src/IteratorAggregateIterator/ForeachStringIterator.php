<?php
declare (strict_types=1);

namespace IteratingThings\IteratorAggregateIterator;

class ForeachStringIterator implements \Iterator
{
    /** @var string */
    protected $str;

    /** @var int */
    protected $ptr;

    public function __construct(string $str)
    {
        $this->str = $str;
        $this->ptr = 0;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function current() : string
    {
        return $this->str[$this->ptr];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     */
    public function next() : void
    {
        $this->ptr++;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     */
    public function key() : int
    {
        return $this->ptr;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     */
    public function valid() : bool
    {
        return $this->ptr < strlen($this->str);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     */
    public function rewind() : void
    {
        $this->ptr = 0;
    }

    public function __toString() : string
    {
        return $this->str;
    }
}
