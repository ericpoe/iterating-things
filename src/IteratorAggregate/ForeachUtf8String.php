<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregate;

class ForeachUtf8String implements \IteratorAggregate
{
    /** @var string */
    protected $encoding;

    /** @var array */
    protected $str;

    public function __construct(string $str)
    {
        $this->encoding = 'UTF-8';
        $this->str = preg_split('//u', mb_convert_encoding($str, $this->encoding), 0, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->str);
    }

    public function __toString() : string
    {
        return implode("", $this->str);
    }
}
