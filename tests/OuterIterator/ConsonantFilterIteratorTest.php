<?php
namespace IteratingThings\tests\OuterIterator;

use IteratingThings\Iterator\ForeachUtf8String;
use IteratingThings\OuterIterator\ConsonantFilterIterator;

class ConsonantFilterIteratorTest extends \PHPUnit_Framework_TestCase
{
    public function testAllVowelStringIsEmpty()
    {
        $str = new ForeachUtf8String("aeiou");

        $it = new ConsonantFilterIterator($str);

        $consonants = "";
        foreach ($it as $char) {
            $consonants .= $char;
        }

        $this->assertEmpty($consonants, "Vowels were not filtered out");
    }

    public function testVowelsInStringAreFilteredOut()
    {
        $str = new ForeachUtf8String("The quick brown fox jumps over the lazy dogs");

        $it = new ConsonantFilterIterator($str);

        $consonants = "";
        foreach ($it as $char) {
            $consonants .= $char;
        }

        $this->assertSame("Th qck brwn fx jmps vr th lzy dgs", $consonants, "Some vowels still exist");
    }
}
