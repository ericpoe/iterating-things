<?php
$SplIterators = [
    "ArrayIterator",
    [
        "RecursiveArrayIterator",
    ],
    'DirectoryIterator (extends "SplFileInfo")',
    [
        "FileSystemIterator",
        [
            "GlobIterator",
            "RecursiveDirectoryIterator",
        ],
    ],
    "EmptyIterator",
    "IteratorIterator",
    [
        "AppendIterator",
        "CachingIterator",
        [
            "RecursiveCachingIterator",
        ],
        "FilterIterator",
        [
            "CallbackFilterIterator",
            [
                "RecursiveCallbackFilterIterator",
            ],
            "RecursiveFilterIterator",
            [
                "ParentIterator",
            ],
            "RegexIterator",
            [
                "RecursiveRegexIterator",
            ],
        ],
        "InfiniteIterator",
        "LimitIterator",
        "NoRewindIterator",
    ],
    "MultipleIterator",
    "RecursiveIterator",
    "SeekableIterator",
    "RecursiveIteratorIterator",
    [
        "RecursiveTreeIterator",
    ],
];

echo "The SPL Iterators are: " . PHP_EOL;

$recursiveArrayIt = new RecursiveArrayIterator($SplIterators);

echoRecursiveLine($recursiveArrayIt);

function echoRecursiveLine(RecursiveArrayIterator $it) : RecursiveArrayIterator
{
    while ($it->valid()) {
        if ($it->hasChildren()) {
            echoRecursiveLine(new RecursiveArrayIterator($it->getChildren()));
        }
        if (is_string($it->current())) {
            echo $it->current() . PHP_EOL;
        }
        $it->next();
    }

    return $it;
}
