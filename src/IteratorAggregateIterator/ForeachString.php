<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregateIterator;

class ForeachString implements \IteratorAggregate
{
    /** @var string */
    protected $str;

    public function __construct(string $str)
    {
        $this->str = $str;
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : ForeachStringIterator
    {
        return new ForeachStringIterator($this->str);
    }

    public function __toString() : string
    {
        return $this->str;
    }
}
