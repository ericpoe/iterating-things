<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregateIterator\ForeachString;

$text = isset($argv[1])
    ? new ForeachString($argv[1])
    : new ForeachString("Iteration can be fun");

$it = $text->getIterator();

foreach ($it as $char) {
    echo $char . " ";
}

echo PHP_EOL;
