<?php
namespace IteratingThings\Tests;


use IteratingThings\Iterator\ForeachString;
use IteratingThings\IteratorAggregate\ForeachString as IAForeachString;
use IteratingThings\IteratorAggregateIterator\ForeachString as ComboForeachString;

class ForeachStringTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider iteratorProvider
     */
    public function testForeach($str, $expected)
    {
        $strArr = [];

        foreach ($str as $char) {
            $strArr[] = $char;
        }

        $this->assertSame($expected, $strArr);
    }

    public function iteratorProvider() : array
    {
        $str = "Iterator";
        $expected = [
            "I",
            "t",
            "e",
            "r",
            "a",
            "t",
            "o",
            "r",
        ];

        return [
            "Iterator" => [new ForeachString($str), $expected],
            "IteratorAggregate" => [new IAForeachString($str), $expected],
            "IteratorAggregateIterator" => [new ComboForeachString($str), $expected],
        ];
    }
}
