<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The quick brown fox jumps over the lazy dogs");

$it = $text->getIterator();
// Start iterating on the 5th char and stop after 11 chars
foreach (new LimitIterator($it, 4, 11) as $char) {
    echo $char;
}

echo PHP_EOL;
