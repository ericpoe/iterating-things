<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregateIterator;

class ForeachUtf8String implements \IteratorAggregate
{
    /** @var string */
    protected $encoding;

    /** @var string */
    protected $str;

    public function __construct(string $str)
    {
        $this->encoding = 'UTF-8';
        $this->str = mb_convert_encoding($str, $this->encoding);
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : ForeachUtf8StringIterator
    {
        return new ForeachUtf8StringIterator($this->str);
    }

    public function __toString() : string
    {
        return $this->str;
    }
}
