<?php

$nums = range(1, 100);

$divisibleBySeven = array_filter($nums, function ($num) {
    return 0 === $num % 7;
});

printf("%d integers between 1 and 100 are divisible by 7: ", count($divisibleBySeven));

foreach ($divisibleBySeven as $value) {
    echo $value . " ";
}

echo PHP_EOL;
