<?php
namespace IteratingThings;

interface FunctionalMethods
{
    public function map(\Closure $closure);

    public function filter(\Closure $closure);

    public function reduce(\Closure $closure, $carry = null);
}
