<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The quick brown fox jumps over the lazy dogs");

$it = $text->getIterator();
$noRewindIt = new NoRewindIterator($it);

echo "First Iterator - First Run:" . PHP_EOL;
foreach ($noRewindIt as $char) {
    echo $char;
}

echo PHP_EOL . "First Iterator - Second Run:" . PHP_EOL;
foreach ($noRewindIt as $char) {
    echo $char;
}

$noRewindIt = new NoRewindIterator($it);

echo "Second Iterator - First Run:" . PHP_EOL;
foreach ($noRewindIt as $char) {
    echo $char;
}

echo PHP_EOL . "Second Iterator - Second Run:" . PHP_EOL;
foreach ($noRewindIt as $char) {
    echo $char;
}
