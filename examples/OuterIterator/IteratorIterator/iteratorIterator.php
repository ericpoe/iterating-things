<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\IteratorAggregate\ForeachUtf8String;

$text = isset($argv[1])
    ? new ForeachUtf8String($argv[1])
    : new ForeachUtf8String("The meetings will continue until morale improves.");

$it = $text->getIterator();

$phpCeoMessage = new class($it) extends IteratorIterator {
    public function current()
    {
        return mb_strtoupper(parent::current());
    }
};

foreach ($phpCeoMessage as $char) {
    echo $char;
}

echo PHP_EOL;
