<?php
declare(strict_types=1);

namespace IteratingThings\IteratorAggregate;

class ForeachString implements \IteratorAggregate
{
    /** @var array */
    protected $str;

    public function __construct(string $str)
    {
        $this->str = str_split($str);
    }

    /**
     * Something that implements \Iterator
     *
     * Iterates over this object
     */
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->str);
    }

    public function __toString() : string
    {
        return implode("", $this->str);
    }
}
