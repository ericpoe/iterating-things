<?php
declare(strict_types=1);

namespace IteratingThings\Iterator;

class SeekableForeachString extends ForeachString implements \SeekableIterator
{
    /**
     * Seeks to a position
     *
     * @link  http://php.net/manual/en/seekableiterator.seek.php
     * @param int $position <p>
     *                      The position to seek to.
     *                      </p>
     * @return void
     * @since 5.1.0
     */
    public function seek($position) : void
    {
        if (!isset($this->str[$position])) {
            throw new \OutOfBoundsException("invalid seek position ($position)");
        }

        $this->ptr = $position;
    }
}
