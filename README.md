[![build status](https://gitlab.com/ericpoe/iterating-things/badges/master/build.svg)](https://gitlab.com/ericpoe/iterating-things/commits/master)
[![coverage report](https://gitlab.com/ericpoe/iterating-things/badges/master/coverage.svg)](https://gitlab.com/ericpoe/iterating-things/commits/master)
# Iterating-Things
Sample code that goes along with my Irritating Strings - Iterating Things talk

## Organization
The `src` folder shows different styles of iterators in PHP. This list is not exhaustive, but it should be a good start to learning more about iterators. Some of this code is referenced during the talk.

The `tests` folder contains the various tests and use-cases for the different iterators in the `src` folder.

The `examples` folder contains examples that can be run from the command line. These examples are used during the talk. More on these examples can be seen in the **Examples** section below.

## Requires

* PHP 7.1+
* [composer](https://getcomposer.org)

## To Run

1. Install requirements via: `composer install`
1. Iterate a string via: `php examples/one-of-the-files.php "example string"`

## To Test

1. `vendor/bin/phpunit`

## Examples
The examples are arranged according to the types of iterators they are. Inner iterators are in the InnerIterator directory; outer iterators are in the OuterIterator directory.

*examples/InnerIterator/*
* ArrayIterator/: Types of array iterators
  * *arrayIterator:* Iterates over a list of SPL Iterators
  * *recursiveArrayIterator:* Recursively iterates over a nested list of SPL Iterators
* Custom: These are basic iterators using `foreach`
  * *basicIterator:* Takes ASCII input and prints it out with a space between each character. Input via command line.
  * *basicUtf8Iterator:* Same as *basicIterator* but with UTF-8! Input via command line.
  * *comboBasicIterator:* Same as *basicIterator* but uses a separate class as the iterator. Input via command line.
  * *functionalIterator:* A *basicUtf8Iterator* with `map`, `filter`, and `reduce`. Counts the number of English vowels in a text. Input via command line.

*examples/OuterIterator/*
* *IteratorIterator/:*
  * *infiniteIterator:* An OuterIterator that iterates over an AggregateIterator that takes in UTF-8 text and cycles through it until 10,000 characters are printed. Input via command line.
  * *iteratorIterator:* An OuterIterator that iterates over an AggregateIterator that takes in UTF-8 text and puts it in upper-case. Input via command line.
  * *limitIterator:* An OuterIterator that iterates over 11 elements of an AggregateIterator, starting with the 5th character of the UTF-8 text. Input via command line.
  * *noRewindIterator:* An OuterIterator that iterates over an AggregateIterator only once.
    * *FilterIterator/:*
      * *callbackFilterIterator:*
* *RecursiveIteratorIterator/:*
  * *recursiveIteratorIterator:* An OuterIterator that iterates over a *RecursiveArrayIterator* of a nested array of the SPL Iterators
  * *recursiveTreeIterator:* Like the *RecursiveIteratorIterator* but treats the iterated structure like a tree. With the added benefit of outputting a tree-like structure!
