<?php
namespace IteratingThings\tests;

use IteratingThings\Iterator\SeekableForeachString;
use IteratingThings\IteratorAggregate\ForeachString;

class SeekableStringTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider seekProvider
     *
     * @param \SeekableIterator $it
     * @param int               $offset
     * @param string            $expected
     */
    public function testSeekWithinBounds(\SeekableIterator $it, int $offset, string $expected)
    {
        $it->seek($offset);

        $this->assertEquals($expected, $it->current());
    }

    /**
     * @expectedException \OutOfBoundsException
     * @dataProvider badSeekProvider
     *
     * @param \SeekableIterator $it
     * @param int               $offset
     */
    public function testSeekOutsideOfBounds(\SeekableIterator $it, int $offset)
    {
        $it->seek($offset);
    }

    public function seekProvider() : array
    {
        return [
            "From SeekableIterator" => [new SeekableForeachString("Iterator"), 2, "e"],
            "From IteratorAggregate::seek" => [(new ForeachString("Iterator"))->getIterator(), 2, "e"],
        ];
    }

    public function badSeekProvider() : array
    {
        return [
            "From SeekableIterator" => [new SeekableForeachString("Iterator"), 8],
            "From IteratorAggregate::seek" => [(new ForeachString("Iterator"))->getIterator(), 8],
        ];
    }
}
