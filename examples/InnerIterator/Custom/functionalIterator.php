<?php
include(__DIR__ . "/../../../vendor/autoload.php");

use \IteratingThings\Iterator\FunctionalString;

$text = isset($argv[1])
    ? new FunctionalString($argv[1])
    : new FunctionalString("The quick brown fox jumps over the lazy dogs");

$encoding = 'UTF-8';
$vowels = mb_convert_encoding("aeiou", $encoding);

$numberOfVowels = $text
    ->map(function ($char) use ($encoding) {
        return mb_strtolower($char, $encoding);
    })->filter(function ($char) use ($encoding, $vowels) {
        return false !== mb_strpos($vowels, $char, null, $encoding);
    })->reduce(function ($carry) {
        return $carry += 1;
    });

printf("The number of vowels in \"%s\" is %d\n", $text, $numberOfVowels);
