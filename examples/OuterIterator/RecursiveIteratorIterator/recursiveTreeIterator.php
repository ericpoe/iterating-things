<?php
$SplIterators = [
    "ArrayIterator",
    [
        "RecursiveArrayIterator",
    ],
    'DirectoryIterator (extends "SplFileInfo")',
    [
        "FileSystemIterator",
        [
            "GlobIterator",
            "RecursiveDirectoryIterator",
        ],
    ],
    "EmptyIterator",
    "IteratorIterator",
    [
        "AppendIterator",
        "CachingIterator",
        [
            "RecursiveCachingIterator",
        ],
        "FilterIterator",
        [
            "CallbackFilterIterator",
            [
                "RecursiveCallbackFilterIterator",
            ],
            "RecursiveFilterIterator",
            [
                "ParentIterator",
            ],
            "RegexIterator",
            [
                "RecursiveRegexIterator",
            ],
        ],
        "InfiniteIterator",
        "LimitIterator",
        "NoRewindIterator",
    ],
    "MultipleIterator",
    "RecursiveIterator",
    "SeekableIterator",
    "RecursiveIteratorIterator",
    [
        "RecursiveTreeIterator",
    ],
];

echo "The SPL Iterators are: " . PHP_EOL;

$arrIt = new RecursiveArrayIterator($SplIterators);
$it = new RecursiveTreeIterator($arrIt, null, null, RecursiveIteratorIterator::LEAVES_ONLY);

foreach ($it as $line) {
    echo $line . PHP_EOL;
}
